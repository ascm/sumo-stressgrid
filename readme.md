# SUMO Stress Grid

Generate a stressful grid scenario for SUMO for benchmarking purposes.
## Requirements

* Python 3.6
  * lxml (optional, but faster than built-in xml)
  * see `requirements.txt`
    ```commandline
    pip3 -r requirements.txt [--user] [--prefix=]
    ```
* SUMO
  * See included submodules, so make sure to use
    ```commandline
    git submodule update --init
    ```
  
## Usage

Simply run
```commandline
SUMO_HOME=./sumo python3 stressgrid.py
```
to generate the SUMO scenario.
(Insert `SUMO_HOME=./sumo` before the command to avoid schema lookups via web.)

Also see
```commandline
python3 stressgrid.py --help
```
for command line arguments, i.e.
```commandline
usage: stressgrid.py [-h] [-x X] [-y Y] [-lanes NUMBER]
                     [-segment-length METERS] [-trips NUMBER]
                     [-duration SECONDS] [-intersection RULE] [-no-teleport]
                     [-prefix STRING] [-sumo-home PATH]

optional arguments:
  -h, --help            show this help message and exit
  -x X                  grid size in x-dim, DEFAULT: 10
  -y Y                  grid size in y-dim, DEFAULT: 10
  -lanes NUMBER         number of lanes, DEFAULT: 4
  -segment-length METERS
                        segment length between intersections (in meter),
                        DEFAULT: 10000
  -trips NUMBER         number of trips from each origin TAZ to destination
                        TAZ, DEFAULT: 1
  -duration SECONDS     duration, DEFAULT: 10
  -intersection RULE    regulation rule of intersections, e.g. "unregulated",
                        "right_before_left", DEFAULT: unregulated
  -no-teleport          disable teleportation (sets time to teleport = -1)
  -prefix STRING        prefix for each generated file
  -sumo-home PATH       location of SUMO's home dir
```

Run SUMO with
```commandline
SUMO_HOME=./sumo ./sumo/bin/sumo-gui -c grid.sumo.cfg -v
# or without GUI
SUMO_HOME=./sumo ./sumo/bin/sumo -c grid.sumo.cfg -v
```
Have fun! :)