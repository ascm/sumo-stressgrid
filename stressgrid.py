# -*- coding: utf-8 -*-
# @cond LICENSE
# #############################################################################
# # LGPL License                                                              #
# #                                                                           #
# # This file is part of the SUMO Stress Grid project.                        #
# # Copyright (c) 2017, Malte Aschermann (malte.aschermann@tu-clausthal.de)   #
# # This program is free software: you can redistribute it and/or modify      #
# # it under the terms of the GNU Lesser General Public License as            #
# # published by the Free Software Foundation, either version 3 of the        #
# # License, or (at your option) any later version.                           #
# #                                                                           #
# # This program is distributed in the hope that it will be useful,           #
# # but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# # GNU Lesser General Public License for more details.                       #
# #                                                                           #
# # You should have received a copy of the GNU Lesser General Public License  #
# # along with this program. If not, see http://www.gnu.org/licenses/         #
# #############################################################################
# @endcond

import os
try:
    import lxml.etree as etree
except ImportError:
    print("lxml not found, running with Python's built-in xml.etree.ElementTree")
    import xml.etree.ElementTree as etree
import argparse
import subprocess
import itertools


def create_scenario(x: int, y: int, lanes, segment_length: float, intersection: str, prefix: str) -> tuple:
    l_nodes = etree.Element('nodes')
    l_edges = etree.Element('edges')

    print('generate grid')
    for i_x in range(x):
        for i_y in range(y):
            etree.SubElement(
                l_nodes,
                'node',
                attrib={
                    'id': f'{i_x}:{i_y}',
                    'x': f'{i_x * segment_length}',
                    'y': f'{i_y * segment_length}',
                    'type': f'{intersection}'
                }
            )
            if i_x < x-1:
                etree.SubElement(
                    l_edges,
                    'edge',
                    attrib={
                        'id': f'{i_x}:{i_y}-{i_x+1}:{i_y}',
                        'from': f'{i_x}:{i_y}',
                        'to': f'{i_x+1}:{i_y}',
                        'numLanes': f'{lanes}',
                    }
                )
                etree.SubElement(
                    l_edges,
                    'edge',
                    attrib={
                        'id': f'{i_x+1}:{i_y}-{i_x}:{i_y}',
                        'from': f'{i_x+1}:{i_y}',
                        'to': f'{i_x}:{i_y}',
                        'numLanes': f'{lanes}',
                    }
                )
            if i_y < y-1:
                etree.SubElement(
                    l_edges,
                    'edge',
                    attrib={
                        'id': f'{i_x}:{i_y+1}-{i_x}:{i_y}',
                        'from': f'{i_x}:{i_y}',
                        'to': f'{i_x}:{i_y+1}',
                        'numLanes': f'{lanes}',
                    }
                )
                etree.SubElement(
                    l_edges,
                    'edge',
                    attrib={
                        'id': f'{i_x}:{i_y}-{i_x}:{i_y+1}',
                        'from': f'{i_x}:{i_y+1}',
                        'to': f'{i_x}:{i_y}',
                        'numLanes': f'{lanes}',
                    }
                )

    print('writing intermediate xml')
    with open(f'{prefix}grid.nod.xml', 'w') as f_nodesxml:
        f_nodesxml.write(
            etree.tostring(l_nodes, encoding='unicode')
        )
    with open(f'{prefix}grid.edg.xml', 'w') as f_edgesxml:
        f_edgesxml.write(
            etree.tostring(l_edges, encoding='unicode')
        )

    return tuple([edge.get('id') for edge in l_edges])


def generate_od(trips: int, duration: int, edge_array: tuple, prefix: str):
    print('generate TAZs')
    l_tazs = etree.Element('tazs')
    for id, i_edge in enumerate(edge_array):
        etree.SubElement(
            l_tazs,
            'taz',
            attrib={'id': f'{id}', 'edges': f'{i_edge}' }
        )
    with open(f'{prefix}grid.tazs.xml', 'w') as f_tazsxml:
        f_tazsxml.write(
            etree.tostring(l_tazs, encoding='unicode')
        )

    print('generate amitran O/D')
    l_demand = etree.Element('demand')
    l_actor_config = etree.SubElement(
        l_demand,
        'actorConfig',
        attrib={'id': 'passenger'}
    )
    l_time_slice = etree.SubElement(
        l_actor_config,
        'timeSlice',
        attrib={'duration': f'{duration*2000}', 'startTime': '0'}
    )
    for i_origin, i_destination in itertools.permutations(range(len(edge_array)), 2):
        etree.SubElement(
            l_time_slice,
            'odPair',
            attrib={'amount': f'{trips}', 'destination': f'{i_origin}', 'origin': f'{i_destination}'}
        )

    print('writing amitran O/D xml')
    with open(f'{prefix}grid.amitran.xml', 'w') as f_amitranxml:
        f_amitranxml.write(
            etree.tostring(l_demand, encoding='unicode')
        )


def od2trips(prefix: str):
    print('running od2trips')
    subprocess.run(
        [
            f'{os.environ["SUMO_HOME"]}/bin/od2trips',
            f'--taz-files={prefix}grid.tazs.xml',
            f'--od-amitran-files={prefix}grid.amitran.xml',
            '--ignore-vehicle-type=true',
            '--different-source-sink=true',
            '--departlane=free',
            '--departpos=random_free',
            '--arrivalpos=random',
            f'--output-file={prefix}grid.trips.xml'
        ],
        stderr=subprocess.STDOUT,
        bufsize=-1,
        close_fds=True
    )


def netconvert(prefix: str):
    print('running netconvert')
    subprocess.run(
        [
            f'{os.environ["SUMO_HOME"]}/bin/netconvert',
            f'--node-files={prefix}grid.nod.xml',
            f'--edge-files={prefix}grid.edg.xml',
            f'--output-file={prefix}grid.net.xml'
        ],
        stderr=subprocess.STDOUT,
        bufsize=-1,
        close_fds=True
    )


def duarouter(prefix: str):
    print('running duarouter')
    subprocess.run(
        [
            f'{os.environ["SUMO_HOME"]}/bin/duarouter',
            '-n', f'{prefix}grid.net.xml',
            f'--route-files={prefix}grid.trips.xml',
            '--remove-loops=true',
            '--routing-threads=8',
            '--departlane=free',
            '--departpos=random_free',
            '--arrivalpos=random',
            f'--output-file={prefix}grid.routes.xml'
        ],
        stderr=subprocess.STDOUT,
        bufsize=-1,
        close_fds=True
    )


def create_config(teleport: bool, prefix: str):
    print('creating config')
    l_configuration = etree.Element('configuration')
    l_input = etree.SubElement(l_configuration, 'input')
    etree.SubElement(
        l_input,
        'net-file',
        attrib={'value': f'{prefix}grid.net.xml'}
    )
    etree.SubElement(
        l_input,
        'route-files',
        attrib={'value': f'{prefix}grid.routes.xml'}
    )
    if not teleport:
        l_processing = etree.SubElement(l_configuration, 'processing')
        etree.SubElement(
            l_processing,
            'time-to-teleport',
            attrib={'value': '-1'}
        )
    l_routing = etree.SubElement(l_configuration, 'routing')
    etree.SubElement(
        l_routing,
        'device.rerouting.probability',
        attrib={'value': '1'}
    )

    with open(f'{prefix}grid.sumo.cfg', 'w') as f_configxml:
        f_configxml.write(
            etree.tostring(
                l_configuration,
                encoding='unicode'
            )
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-x', dest='x', type=int, default=10, help='grid size in x-dim, DEFAULT: 10')
    parser.add_argument('-y', dest='y', type=int, default=10, help='grid size in y-dim, DEFAULT: 10')
    parser.add_argument('-lanes', dest='lanes', type=int, default=4, metavar='NUMBER', help='number of lanes, DEFAULT: 4')
    parser.add_argument('-segment-length', dest='segment_length', metavar='METERS', type=float, default=10000, help='segment length between intersections (in meter), DEFAULT: 10000')
    parser.add_argument('-trips', dest='trips', type=int, metavar='NUMBER', default=1, help='number of trips from each origin TAZ to destination TAZ, DEFAULT: 1')
    parser.add_argument('-duration', dest='duration', type=int, metavar='SECONDS', default=10, help='duration, DEFAULT: 10')
    parser.add_argument('-intersection', dest='intersection', type=str, metavar='RULE', default='unregulated', help='regulation rule of intersections, e.g. "unregulated", "right_before_left", DEFAULT: unregulated')
    parser.add_argument('-no-teleport', dest='teleport', action='store_false', default=True, help='disable teleportation (sets time to teleport = -1)')
    parser.add_argument('-prefix', dest='prefix', type=str, default="", metavar='STRING', help='prefix for each generated file')
    
    args = parser.parse_args()

    generate_od(
        trips=args.trips,
        edge_array=create_scenario(
            x=args.x,
            y=args.y,
            lanes=args.lanes,
            segment_length=args.segment_length,
            intersection=args.intersection,
            prefix=args.prefix
        ),
        duration=args.duration,
        prefix=args.prefix
    )
    netconvert(prefix=args.prefix)
    od2trips(prefix=args.prefix)
    duarouter(prefix=args.prefix)
    create_config(teleport=args.teleport, prefix=args.prefix)
